const express = require('express');
const cors = require('cors');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const app = express();

app.use(cors({ origin: true }));
admin.initializeApp({
    credential: admin.credential.cert(
        {
            "type": "service_account",
            "project_id": "mirror-app-ef1d2",
            "private_key_id": "a2a2b57e4eac74ab35784f99a0dbf7c60c9e9b95",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDUCaFoOeh9SCtY\nhrbHJwszLNxHisHdC6h0x/FU/+Dne8jNxsrXhZUiEHq+1R+Wo9ah8O1//bmc3Y6A\nz/gXBqiF7Qb+htNZb5ws6y5nTV7EmW4UCp4/NfazgRXwepPiuqfRLmLiOsGqgaqQ\n8M+fqM9FHA8sEwY78A5LfMyXM9MvrPM/hBdWgfzGZnLXMk0dGvtzucZoikxReC3n\nvyg5WTQ93yyE+zTIVghsCEZEA8pF+hSrB90JDs70wMDlGkSmf2RWI+iY2UwrZ6ma\n8pO9DW/fOa/A3j8kIqkJDAqaAmPfh5878W3KWu14DCz9L/yTG3re85y43t7cj+j0\njm0k6ckHAgMBAAECggEACJsrdfODwDIBsGInYQXwqm8TscOsArDKcZL79Js+Iacg\ni1maOg0t9Gf+02gLFC4qNAB3Y06FDhzf/9JxG14l0OnB8uq+AU9KY3hYNdEqG4iu\nsxmLOr2R+c8veMdA4V76GL9tPAlueuEwbxmXE7/iScfNxdgv1Jb1vZKfKzPrSnAD\npFgQ6NqcNr1WJFtq9zB7yLhyqAJsLNj9FodMO69lsroOirY60wQl4zgnOB4iPH4P\npE2wsZkoQWkIEW00NqX9lRs78g0dzr4VOalqvQYNWr1teXLQu0PjTJzUiZzr7d8e\nB85OlA/VL25Gx8Lg1fBXhhte5d1wkwgh7FMV1FmeQQKBgQD9nZawMu75y51JgzJp\nAt45ch0vkQRP5eUY63mWi+eT6JGWUc4a76SvHG+d4vAPcKM4ssMloxU+lfWTjGS3\n2XQ1AOMLdhOA+qt4u8NKCx1HOj0PDVK6mCsW2UKIRoqTQr9p9M5l9jc+LBE3DcPW\n8wVPRWZxxYaqVp5zGhDTOesA+QKBgQDWB/hyNt5ci8R1ALKUKhLlTpTzMFiK7THi\nmhG34v08Cd/AFjrP0FKPpslQMl3NDw4WpDfqqrR6Lioe3cij6bQpQrJTlJ6Kh79G\nEb7XIFijwZdcq9FfAVIs5LT+0ZhjZjWL/4BY0+hH0BMAIhPu4c1FiqTwUT4tjGtm\ny8PXkPeZ/wKBgFSaCzh/5SRC/hcVwu0w3bzACmhhsdSNVh03//ludjPq8xX3+A8Y\nPhGEJN/AYmBkLK6vQ1X8zwtvrtonjTmfOpqrzxF1tA/kjurZh83dY0jtbozk3uIL\n/OPEWeNn1LZHH99SoL92oPsGbD+3QcskinLlLcVqJQARoQ24dkBgI7rRAoGAPJ6x\nTZggztF/aSwzaCeqhmwFYAO3cFHSKT49+nk+BD0/tGYXbjiwyaMRrbc2Phgfg2Yr\nuDl1OI3OvENQyqwsrOsJGwcjCFV+ZVgtVz0AHhF8z3CSsnfrUG/Om8W9hZmzXN2W\n+pG6XmyelRER2Dz4KNQl9zLri3cUDF92Lp4Oov0CgYEAxsdLEf7Mm3OzpDSTSuOH\nthBGz6XG7bBtfdiI7ikA754niOYcQ6sUAx1vjJYGmUKkfdrJ+D7pCpcyUhfCbd1F\no0xEUUnUX9X6qt2MovDvw4WpNtc+shoKPvzixut4s2bK8pbssLz4ESNfVl1zKlTh\ncRhYxn3qiBYmFt57lWLiZ0k=\n-----END PRIVATE KEY-----\n",
            "client_email": "firebase-adminsdk-var0b@mirror-app-ef1d2.iam.gserviceaccount.com",
            "client_id": "117332630034484348391",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-var0b%40mirror-app-ef1d2.iam.gserviceaccount.com"
          }
    ),
    databaseURL: "https://mirror-app-ef1d2.firebaseio.com"
});
const db = admin.firestore();

//HttpGet
//http://localhost:5000/mirror-app-ef1d2/us-central1/get?root=entries
//https://us-central1-mirror-app-ef1d2.cloudfunctions.net/get?root=entries
exports.get = functions.https.onRequest(async (request, response) => {
    var root = String(request.query.root);
    const snapshot = await db.collection(root).get();
    entries = snapshot.empty ? [] : snapshot.docs.map(doc => Object.assign(doc.data(), { id: doc.id }));
    response.send(entries);
});

//HttpPost
//http://localhost:5000/mirror-app-ef1d2/us-central1/put?root=entries&id=123
//https://us-central1-mirror-app-ef1d2.cloudfunctions.net/put?root=entries&id=Manickam
//Body: {"Name":"Hello1234","Class":"123","School":"GHSS"}
exports.put = functions.https.onRequest(async (request, response) => {
    var data = request.body;
    var root = String(request.query.root);
    var id = String(request.query.id);
    await db.collection(root)
        .doc(id)
        .set(data, { merge: true });
    response.send(data);
});
