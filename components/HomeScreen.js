import React, { Component } from 'react';
import {
  StyleSheet,
  Button,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  TouchableHighlight, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, Platform
} from 'react-native';
import bgContainer from './../assets/bgContainer.jpg';
import happy from './../assets/happy.png';
import veryHappy from './../assets/very-happy.png';
import veryAngry from './../assets/very-sad.png';
import angry from './../assets/sad.png';

import logo from './../assets/logo.png';
import Icon from 'react-native-vector-icons/Ionicons';
const { width: WIDTH } = Dimensions.get('window');
import { createStackNavigator, createAppContainer } from 'react-navigation';
//import Fire from './../services/Fire';

export default class HomeScreen extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/home-icon.png')}
        style={[styles.icon, { tintColor: tintColor }]}
      />
    ),
  };

  state = {
    name: '',
  };

  onPressThanks = () =>
    this.props.navigation.navigate('Main', { name: '' });

  onChangeText = name => this.setState({ name });

  postAction = (action, actionBy) => {
    const actionOn = new Date();
    const id = actionOn.getTime();
    console.log(actionBy);
    const url = 'https://us-central1-mirror-app-ef1d2.cloudfunctions.net/put?root=mooddata&id=' + id;
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        action: action,
        actionBy: actionBy,
        actionOn: actionOn
      }),
    });
    alert('Thank you!');
    this.props.navigation.navigate('Main', { name: '' });
  }

  getActions = () => {
    return fetch('https://us-central1-mirror-app-ef1d2.cloudfunctions.net/get?root=mooddata')
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  onPressVeryHappy = () => {   
    this.postAction('Excelent', (this.props.navigation.state.params.name !== '' && this.props.navigation.state.params.name != undefined) ? this.props.navigation.state.params.name : 'anonymous');
  }

  onPressHappy = () => {
    this.postAction('Normal', (this.props.navigation.state.params.name !== '' && this.props.navigation.state.params.name != undefined) ? this.props.navigation.state.params.name : 'anonymous');
  }

  onPressAngry = () => {
    this.postAction('Bad', (this.props.navigation.state.params.name !== '' && this.props.navigation.state.params.name != undefined) ? this.props.navigation.state.params.name : 'anonymous');
  }

  onPressVeryAngry = () => {
    this.postAction('Worst', (this.props.navigation.state.params.name !== '' && this.props.navigation.state.params.name != undefined) ? this.props.navigation.state.params.name : 'anonymous');
  }

  render() {
    return (
      <ImageBackground source={bgContainer} style={styles.backgroundContainer}>
        <View style={styles.greetingsRow}>
          <Text style={styles.greetingText} >Hello {this.props.navigation.state.params.name}</Text>
        </View>
        <View style={styles.rowContainer}>
          <TouchableHighlight onPress={this.onPressVeryHappy}>
            <View style={styles.logoContainer} >
              <Image source={veryHappy} style={styles.logo} />
              <Text style={styles.logoText} >Excelent</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.onPressHappy}>
            <View style={styles.logoContainer}>
              <Image source={happy} style={styles.logo} />
              <Text style={styles.logoText} >Happy</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style={styles.rowContainer}>
          <TouchableHighlight onPress={this.onPressAngry}>
            <View style={styles.logoContainer}>
              <Image source={angry} style={styles.logo} />
              <Text style={styles.logoText} >Bad</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.onPressVeryAngry}>
            <View style={styles.logoContainer}>
              <Image source={veryAngry} style={styles.logo} />
              <Text style={styles.logoText} >Worst</Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style={styles.thanks}>
          <Button title="No Thanks!" onPress={this.onPressThanks} style={styles.thanksButton} />
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignContent: 'center'
  },
  greetingsRow: {
    textAlign: 'center',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 80,
    marginLeft: 30
  },
  greetingText: {
    color: 'white',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 30
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    height: 120,
    width: 120
  },
  logoText: {
    color: 'black',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },
  textContainer: {
    paddingTop: 20
  },
  input: {
    width: WIDTH - 55,
    borderRadius: 25,
    height: 45,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 25
  },
  inputIcon: {
    position: 'absolute',
    top: 28,
    left: 37
  },
  thanks: {
    alignContent: 'center',
    width: WIDTH - 0,
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 50,
    borderRadius: 50
  },
  thanksButton: {
    borderRadius: 25
  }

});
