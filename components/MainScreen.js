import React, { Component } from 'react';
import {
  StyleSheet,
  Button,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
  Dimensions
} from 'react-native';
import bgContainer from './../assets/bgContainer.jpg'
import logo from './../assets/logo.png';
import Icon from 'react-native-vector-icons/Ionicons';
const { width: WIDTH } = Dimensions.get('window');
import { createStackNavigator, createAppContainer } from 'react-navigation';

export default class MainScreen extends Component {

  state = {
    name: '',
  };

  onPress = () =>
    this.props.navigation.navigate('Home', { name: this.state.name });

  onChangeText = name => this.setState({ name });


  static navigationOptions = {
    drawerLabel: 'Main',
    drawerIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/home-icon.png')}
        style={[styles.icon, { tintColor: tintColor }]}
      />
    ),
  };

  render() {
    return (
      <ImageBackground source={bgContainer} style={styles.backgroundContainer}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
          <Text style={styles.logoText} >Mirror</Text>
        </View>
        <View style={styles.textContainer}>
          <Icon name={'ios-man'} size={28} color={'#00FF00'}
            style={styles.inputIcon} />

          <TextInput
            onChangeText={this.onChangeText}
            value={this.state.name}
            style={styles.input}
            placeholder={'Hello, I am...'}
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent' />
        </View>

        <View style={styles.allowMe}>          
            <Button title="Allow me" onPress={this.onPress} />          
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignContent: 'center'
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    height: 120,
    width: 120
  },
  logoText: {
    color: 'black',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },
  textContainer: {
    paddingTop: 20
  },
  input: {
    width: WIDTH - 55,
    borderRadius: 25,
    height: 45,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 25
  },
  inputIcon: {
    position: 'absolute',
    top: 28,
    left: 37
  },
  allowMe: {
    alignContent: 'center',
    width: WIDTH - 0,
    alignItems: 'center',
    paddingTop: 15,
    borderRadius: 25
  }

});
